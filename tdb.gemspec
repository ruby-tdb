manifest = File.exist?('.manifest') ?
  IO.readlines('.manifest').map!(&:chomp!) : `git ls-files`.split("\n")

Gem::Specification.new do |s|
  s.name = %q{tdb}
  s.version = (ENV['VERSION'] || '0.6.4').dup
  s.homepage = 'https://yhbt.net/ruby-tdb/'
  s.authors = ["Ruby tdb hackers"]
  s.description = File.read('README').split("\n\n")[1]
  s.email = %q{ruby-tdb@yhbt.net}
  s.extra_rdoc_files = IO.readlines('.document').map!(&:chomp!).keep_if do |f|
    File.exist?(f)
  end
  s.files = manifest
  s.summary = 'Trivial Database bindings for Ruby'
  s.test_files = Dir['test/test_*.rb']
  s.extensions = %w(ext/tdb/extconf.rb)
  s.add_development_dependency('test-unit', '>= 2.0', '< 4.0')
  s.licenses = %w(LGPL-2.1+)
end
