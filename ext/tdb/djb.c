#include "rbtdb.h"

unsigned int rbtdb_djb2(TDB_DATA *data)
{
	unsigned char *key = data->dptr;
	size_t len = data->dsize;
	unsigned int hash = 5381;
	unsigned int i;

	for (i = 0; i < len; ++i)
		hash = ((hash << 5) + hash) + key[i]; /* (hash*33) + key[i] */

	return hash;
}
unsigned int rbtdb_djb3(TDB_DATA *data)
{
	unsigned char *key = data->dptr;
	size_t len = data->dsize;
	unsigned int hash = 5381;
	unsigned int i;

	for (i = 0; i < len; ++i)
		hash = ((hash << 5) + hash) ^ key[i]; /* (hash*33) ^ key[i] */

	return hash;
}
