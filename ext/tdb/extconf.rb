require 'mkmf'

have_func('rb_thread_blocking_region')
have_func('rb_thread_call_with_gvl')
have_func('rb_thread_call_without_gvl', 'ruby/thread.h')

dir_config('tdb')
have_header('tdb.h') or abort 'tdb.h missing'
have_library('tdb') or abort 'libtdb missing'
have_func('tdb_jenkins_hash')
have_func('tdb_repack')
have_const('TDB_ERR_NESTING', 'tdb.h')
have_header('pthread.h')
have_library('pthread')

create_makefile('tdb_ext')
