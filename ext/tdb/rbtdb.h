#ifndef RBTDB_H
#define RBTDB_H
#include <ruby.h>
#include <tdb.h>

unsigned int rbtdb_siphash24(TDB_DATA *key);
unsigned int rbtdb_murmur1(TDB_DATA *key);
unsigned int rbtdb_murmur1_aligned(TDB_DATA *key);
unsigned int rbtdb_murmur2(TDB_DATA *key);
unsigned int rbtdb_murmur2a(TDB_DATA *key);
unsigned int rbtdb_murmur2_neutral(TDB_DATA *key);
unsigned int rbtdb_murmur2_aligned(TDB_DATA *key);
unsigned int rbtdb_murmur3a(TDB_DATA *key);
unsigned int rbtdb_murmur3f(TDB_DATA *key);
unsigned int rbtdb_fnv1a(TDB_DATA *key);
unsigned int rbtdb_djb2(TDB_DATA *key);
unsigned int rbtdb_djb3(TDB_DATA *key);
#ifdef HAVE_TDB_JENKINS_HASH
#  define rbtdb_jenkins_lookup3 tdb_jenkins_hash
#else
unsigned int rbtdb_jenkins_lookup3(TDB_DATA *key);
#endif
#define rbtdb_default 0

void rbtdb_init_tdb_hash_functions(void);
#endif /* RBTDB_H */
