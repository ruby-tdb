#include "rbtdb.h"

#define FNV1A_32A_INIT (unsigned int)0x811c9dc5
#define FNV_32_PRIME (unsigned int)0x01000193

unsigned int rbtdb_fnv1a(TDB_DATA * data)
{
	unsigned char *bp = data->dptr;
	unsigned char *be = bp + data->dsize;
	unsigned int h = FNV1A_32A_INIT;

	/* FNV-1a hash each octet in the buffer */
	while (bp < be) {

		/* xor the bottom with the current octet */
		h ^= (unsigned)*bp++;

		/* multiply by the 32 bit FNV magic prime mod 2^32 */
#if defined(NO_FNV_GCC_OPTIMIZATION)
		h *= FNV_32_PRIME;
#else
		h += (h << 1) + (h << 4) + (h << 7) + (h << 8) + (h << 24);
#endif
	}

	/* return our new hash value */
	return h;
}
