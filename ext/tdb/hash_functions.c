#include "rbtdb.h"

#define HASH_FN(fn) \
static VALUE fn(VALUE self,VALUE str) \
{ \
	TDB_DATA data; \
	StringValue(str); \
	data.dptr = (unsigned char *)RSTRING_PTR(str); \
	data.dsize = RSTRING_LEN(str); \
	return UINT2NUM(rbtdb_##fn(&data)); \
}

HASH_FN(siphash24)
HASH_FN(murmur1)
HASH_FN(murmur1_aligned)
HASH_FN(murmur2)
HASH_FN(murmur2a)
HASH_FN(murmur2_neutral)
HASH_FN(murmur2_aligned)
HASH_FN(murmur3a)
HASH_FN(murmur3f)
HASH_FN(fnv1a)
HASH_FN(djb2)
HASH_FN(djb3)
HASH_FN(jenkins_lookup3)

#define HASH_M(fn) rb_define_method(mHashFunctions, "tdb_hash_"#fn, fn, 1)
void rbtdb_init_tdb_hash_functions(void)
{
	VALUE cTDB = rb_const_get(rb_cObject, rb_intern("TDB"));
	VALUE mHashFunctions = rb_define_module_under(cTDB, "HashFunctions");

	HASH_M(siphash24);
	HASH_M(murmur1);
	HASH_M(murmur1_aligned);
	HASH_M(murmur2);
	HASH_M(murmur2a);
	HASH_M(murmur2_neutral);
	HASH_M(murmur2_aligned);
	HASH_M(murmur3a);
	HASH_M(murmur3f);
	HASH_M(fnv1a);
	HASH_M(djb2);
	HASH_M(djb3);
	HASH_M(jenkins_lookup3);
}
