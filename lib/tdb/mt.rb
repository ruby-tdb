# -*- encoding: binary -*-

# WARNING: this is not recommended, it is still possible to break this
require 'thread'
module TDB::MT
  def initialize
    super
    @lock = Mutex.new
  end

  wrap_methods = %w(
    close closed? fetch [] store []= insert! modify! insert modify
    key? has_key? include? member?
    nuke! delete
    lockall trylockall unlockall
    lockall_read trylockall_read unlockall_read
    lockall_mark lockall_unmark
    clear each
  )
  wrap_methods << :repack if TDB.method_defined?(:repack)
  wrap_methods.each do |meth|
    eval "def #{meth}(*args); @lock.synchronize { super }; end"
  end

  def threadsafe?
    true
  end

  def self.extended(obj)
    obj.instance_eval { @lock = Mutex.new unless defined?(@lock) }
  end

  def self.included(klass)
    ObjectSpace.each_object(klass) { |obj|
      obj.instance_eval { @lock = Mutex.new unless defined?(@lock) }
    }
  end
end
