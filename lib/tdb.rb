# -*- encoding: binary -*-
require 'tdb_ext'
class TDB
  autoload :MT, 'tdb/mt'

  # makes the current TDB object thread-safe (DANGEROUS)
  # Do not use this method yet, it has problems
  def threadsafe!
    extend MT
  end

  # will return true when TDB::MT is included in TDB or the TDB
  # object is extended by TDB
  def threadsafe?
    false
  end
end
