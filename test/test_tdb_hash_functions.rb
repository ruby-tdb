# -*- encoding: binary -*-
require 'test/unit'
$-w = true
require 'tdb'

class TestHashFunctions < Test::Unit::TestCase
  include TDB::HashFunctions

  def test_hashes
    TDB::HASHES.each do |name,_|
      next if :default == name
      assert_kind_of Integer, __send__("tdb_hash_#{name}", "hello")
    end
  end

  def test_distro_sequential
    TDB::HASHES.each do |name,_|
      next if :default == name
      m = method "tdb_hash_#{name}"
      tmp = Hash.new { |h,k| h[k] = 0 }
      (1..100000).each do |s|
        tmp[m.call(s.to_s) % 7] += 1
      end
      assert_equal 7, tmp.size
      tmp.each_value do |v|
        assert_in_delta 100000/7.0, v, 1000
      end
    end
  end

  def test_distro_random
    TDB::HASHES.each do |name,_|
      next if :default == name
      m = method "tdb_hash_#{name}"
      tmp = Hash.new { |h,k| h[k] = 0 }
      100000.times do
        tmp[m.call(rand.to_s) % 7] += 1
      end
      assert_equal 7, tmp.size
      tmp.each_value do |v|
        assert_in_delta 100000/7.0, v, 1000
      end
    end
  end
end
